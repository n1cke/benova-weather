import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'

/** @typedef {Location.LocationData['coords']} Coordinates */

/**
 * @typedef {object} LocationState
 * @prop {Coordinates} coordinates
 * @prop {'undetermined' | 'denied' | 'granted'} permission
 * @prop {boolean} hasServicesEnabled
 */

/** @type {LocationState} */
const state = {
    coordinates: null,
    permission: 'undetermined',
    hasServicesEnabled: false
}

const reducers = {
    /** @type {(state: LocationState, coordinates: Coordinates) => LocationState} */
    setLocationCoordinates(state, coordinates) {
        return { ...state, coordinates }
    },
    /** @type {(state: LocationState, status: 'granted' | 'denied') => LocationState} */
    setLocationPermission(state, permission) {
        return { ...state, permission }
    }
}

const effects = dispatch => ({
    /** @type {(payload?: LocationState, state?: import("../store").State) => Promise<void>} */
    async getLocationCoordinatesAsync(payload, state) {
        try {
            const { coords } = await Location.getCurrentPositionAsync({})
            dispatch.location.setLocationCoordinates(coords)
        } catch (error) {
            dispatch.location.setLocationCoordinates(null)
        }
    },

    async askLocationPermissionAsync() {
        const { status } = await Permissions.askAsync(Permissions.LOCATION)
        dispatch.location.setLocationPermission(status)
    },
    async getLocationPermissionAsync() {
        const { status } = await Permissions.getAsync(Permissions.LOCATION)
        dispatch.location.setLocationPermission(status)
    },
    async getLocationServicesEnabledAsync() {
        const hasServicesEnabled = await Location.hasServicesEnabledAsync()
    }
})

export default { state, reducers, effects }

export { default as settings } from './settings'
export { default as weather } from './weather'
export { default as location } from './location'

/**
 * @typedef {object} SettingsState
 * @prop {'C' | 'F'} temperatureFormat
 * @prop {'24 ч' | '12 ч'} timeFormat
 * @prop {'мм' | 'дюйм'} precipationFormat
 * @prop {'км/ч' | 'миль/ч' | 'м/сек'} windSpeedFormat
 * @prop {'мм рт.ст' | 'гПа' | 'Банкомат' | 'мбар'} pressureFormat
 */

/** @type {SettingsState} */
const state = {
    temperatureFormat: 'C',
    timeFormat: '24 ч',
    precipationFormat: 'мм',
    windSpeedFormat: 'км/ч',
    pressureFormat: 'мм рт.ст'
}

const reducers = {
    /** @type {(state: SettingsState, temperatureFormat: SettingsState['temperatureFormat']) => SettingsState} */
    setTemperatureFormat(state, temperatureFormat) {
        return { ...state, temperatureFormat }
    },
    /** @type {(state: SettingsState, timeFormat: SettingsState['timeFormat']) => SettingsState} */
    setTimeFormat(state, timeFormat) {
        return { ...state, timeFormat }
    },
    /** @type {(state: SettingsState, precipationFormat: SettingsState['precipationFormat']) => SettingsState} */
    setPrecipationFormat(state, precipationFormat) {
        return { ...state, precipationFormat }
    },
    /** @type {(state: SettingsState, windSpeedFormat: SettingsState['windSpeedFormat']) => SettingsState} */
    setWindSpeedFormat(state, windSpeedFormat) {
        return { ...state, windSpeedFormat }
    },
    /** @type {(state: SettingsState, pressureFormat: SettingsState['pressureFormat']) => SettingsState} */
    setPressureFormat(state, pressureFormat) {
        return { ...state, pressureFormat }
    }
}

const effects = dispatch => ({})

export default { state, reducers, effects }

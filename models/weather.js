/**
 * @typedef {object} Weather
 * @prop {{lon: number, lat: number}} coord
 * @prop {Array<{ id: number, main: string, description: string, icon: string}>} weather
 * @prop {string} base
 * @prop {{temp: number, pressure: number, humidity: number, temp_min: number, temp_max: number, sea_level: number, grnd_level: number}} main
 * @prop {number} visibility
 * @prop {{speed: number, deg: number}} wind
 * @prop {{all: number}} clouds
 * @prop {number} dt
 * @prop {{type: number, id: number, message: number, country: string, sunrise: number, sunset: number}} sys
 * @prop {number} timezone
 * @prop {number} id
 * @prop {string} name
 * @prop {number} cod
 */

/**
 * @typedef {object} WeatherState
 * @prop {Weather} weather
 * @prop {boolean} isWeatherLoading
 */

/** @type {WeatherState} */
const state = {
    weather: null,
    isWeatherLoading: false
}

const reducers = {
    /** @type {(state: WeatherState) => WeatherState} */
    requestWeather(state) {
        return { ...state, isWeatherLoading: true }
    },
    /** @type {(state: WeatherState, payload: Weather) => WeatherState} */
    setWeather(state, weather) {
        return { ...state, weather, isWeatherLoading: false }
    },
    /** @type {(state: WeatherState, message: string) => WeatherState} */
    failWeather(state, message) {
        alert(message)
        return { ...state, weather: null, isWeatherLoading: false }
    }
}

const effects = dispatch => ({
    /** @type {(payload?: WeatherState, state?: import("../store").State) => Promise<void>} */
    async fetchWeatherAsync(payload, state) {
        if (!state.location.coordinates) {
            return undefined
        }
        const { latitude, longitude } = state.location.coordinates
        dispatch.weather.requestWeather()
        try {
            const response = await fetch(
                `http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=b3a75c5d38f739080ff26ff2b5ea236e&lang=ru&units=metric`
            )
            if (response.status == 200) {
                const data = await response.json()
                if (data.cod == 200) {
                    dispatch.weather.setWeather(data)
                } else if (data.error) {
                    dispatch.weather.failWeather(
                        `Во время запроса произошла ошибка ${data.error}`
                    )
                } else {
                    dispatch.weather.failWeather(
                        `Во время запроса произошла ошибка ${data.cod}: ${data.message}`
                    )
                }
            } else {
                dispatch.weather.failWeather(
                    `Во время запроса произошла ошибка ${response.status}`
                )
            }
        } catch (error) {
            dispatch.weather.failWeather(error.message)
        }
    },
    /** @type {(payload?: WeatherState, state?: import("../store").State) => Promise<void>} */
    async loadWeatherAtStartAsync(payload, state) {
        await dispatch.location.getLocationPermissionAsync()
        await dispatch.location.getLocationServicesEnabledAsync()
        await dispatch.location.getLocationCoordinatesAsync()
        await dispatch.weather.fetchWeatherAsync()
    }
})

export default { state, reducers, effects }

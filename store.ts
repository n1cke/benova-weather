import { init, RematchRootState } from '@rematch/core'
import * as models from './models/index'

export const store = init({
    models
})

export type Store = typeof store
export type Dispatch = typeof store.dispatch
export type State = RematchRootState<typeof models>

import React from 'react'
import { connect } from 'react-redux'
import {
    Text,
    Button,
    StyleSheet,
    Image,
    ActivityIndicator
} from 'react-native'
import { View } from 'react-native-animatable'
import { Entypo } from '@expo/vector-icons'

/** @param {import('../store').State} state */
export const mapState = state => ({
    weather: state.weather.weather,
    isWeatherLoading: state.weather.isWeatherLoading,
    coordinates: state.location.coordinates,
    permission: state.location.permission,
    hasServicesEnabled: state.location.hasServicesEnabled
})

/** @param {import('../store').Dispatch} dispatch */
export const mapDispatch = dispatch => ({
    fetchWeatherAsync: () => dispatch.weather.fetchWeatherAsync(),
    askLocationPermissionAsync: () =>
        dispatch.location.askLocationPermissionAsync(),
    loadWeatherAtStartAsync: () => dispatch.weather.loadWeatherAtStartAsync()
})

// @ts-ignore
const state = mapState({ weather: {}, location: {} })
// @ts-ignore
const dispatch = mapDispatch({})

/** @type {(props: typeof state & typeof dispatch) => JSX.Element} */
function Weather(props) {
    if (props.isWeatherLoading) {
        return (
            <View>
                <ActivityIndicator
                    size="large"
                    color="#00ccff"
                    style={{ marginTop: 32 }}
                />
            </View>
        )
    }
    if (!props.weather) {
        return (
            <View>
                {props.permission !== 'granted' && (
                    <View style={{ marginTop: 16 }}>
                        <Button
                            title="ВКЛЮЧИТЬ ДОСТУП К ГЕОЛОКАЦИИ"
                            onPress={props.askLocationPermissionAsync}
                        />
                    </View>
                )}
                {props.permission === 'granted' && (
                    <View style={{ marginTop: 16 }}>
                        <Button
                            title="ОБНОВИТЬ ПРОГНОЗ ПОГОДЫ"
                            onPress={props.loadWeatherAtStartAsync}
                        />
                    </View>
                )}
            </View>
        )
    }

    const { weather } = props

    return (
        <View style={styles.container}>
            <View animation="fadeIn" style={styles.locationName}>
                <Entypo name="location-pin" size={36} color="white" />
                <Text style={styles.locationNameText}>{weather.name}</Text>
            </View>
            {weather.weather.map(e => (
                <View animation="slideInDown" key={e.id} style={styles.weather}>
                    <Image
                        resizeMethod="resize"
                        style={styles.image}
                        source={{
                            uri: `http://openweathermap.org/img/wn/${e.icon}@2x.png`
                        }}
                    />
                    <Text style={styles.weatherDescription}>
                        {e.description}
                    </Text>
                </View>
            ))}
            <View animation="lightSpeedIn" delay={0}>
                <Text style={styles.text}>
                    Температура: {Math.floor(weather.main.temp)} °C
                </Text>
            </View>
            <View animation="lightSpeedIn" delay={100}>
                <Text style={styles.text}>
                    Влажность: {weather.main.humidity}%
                </Text>
            </View>
            <View
                animation="lightSpeedIn"
                delay={200}
                style={{
                    flexDirection: 'row'
                }}
            >
                <Text style={styles.text}>Ветер:</Text>
                <Entypo
                    name="arrow-up"
                    size={18}
                    color="white"
                    style={{
                        transform: [{ rotate: `${weather.wind.deg}deg` }]
                    }}
                />
                <Text style={styles.text}>{weather.wind.speed} км/ч</Text>
            </View>
            <View animation="lightSpeedIn" delay={300}>
                <Text style={styles.text}>
                    Давление: {weather.main.pressure} гПа
                </Text>
            </View>

            <View animation="lightSpeedIn" delay={400}>
                <Text style={styles.text}>
                    Облачность: {weather.clouds.all}%
                </Text>
            </View>
            <View animation="fadeInUp" style={{ marginTop: 16 }}>
                <Button
                    title="ОБНОВИТЬ ПРОГНОЗ ПОГОДЫ"
                    onPress={props.fetchWeatherAsync}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {},
    locationName: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    locationNameText: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold'
    },
    weather: {
        marginBottom: 24,
        alignItems: 'center'
    },
    weatherDescription: {
        color: 'white',
        fontSize: 18
    },
    text: {
        color: 'white',
        fontSize: 16
    },
    image: {
        width: 128,
        height: 128
    }
})

export default connect(mapState, mapDispatch)(Weather)

import React from 'react'
import { init } from '@rematch/core'
import { Provider } from 'react-redux'
import * as models from './models/index'
import { StyleSheet, View } from 'react-native'

import { LinearGradient } from 'expo-linear-gradient'
import Weather from './components/Weather'

// generate Redux store
export const store = init({
    models
})

store.dispatch.weather.loadWeatherAtStartAsync()

export default function App() {
    return (
        <Provider store={store}>
            <View style={styles.container}>
                <LinearGradient
                    style={styles.gradient}
                    colors={['#4c669f', '#f92f6a']}
                >
                    <Weather />
                </LinearGradient>
            </View>
        </Provider>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    gradient: {
        width: '100%',
        height: '100%',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        padding: 24
    }
})
